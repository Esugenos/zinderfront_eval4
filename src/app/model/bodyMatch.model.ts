export class BodyMatchModel {

  match: boolean;

  constructor(match: boolean) {
    this.match = match;
  }
}
