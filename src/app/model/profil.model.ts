export class ProfilModel {

  id: string;
  nom: string;
  prenom: string;
  photoUrl: string;
  interets: string [];

  constructor(id: string, nom: string, prenom: string, photoUrl: string, interets: string[]) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.photoUrl = photoUrl;
    this.interets = interets;
  }
}

