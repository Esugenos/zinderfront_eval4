export class BodyCreateProfilModel {

  nom: string;
  prenom: string;
  photoUrl: string;
  interets: string [];

  constructor(nom: string, prenom: string, photoUrl: string, interets: string[]) {
    this.nom = nom;
    this.prenom = prenom;
    this.photoUrl = photoUrl;
    this.interets = interets;
  }
}
