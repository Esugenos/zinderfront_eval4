import {Injectable} from '@angular/core';
import {ProfilModel} from './profil.model';

@Injectable()
export class ListProfilModel {

  profils: ProfilModel[];

  constructor(listProfil) {
    this.profils = listProfil;
  }
}
