import {Component, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinder.service';
import {BodyCreateProfilModel} from '../../model/bodyCreateProfil.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  nom: string;
  prenom: string;
  photoUrl: string;
  interet1: string;
  interet2: string;
  interet3: string;
  disabled = 'disabled';

  constructor(private service: ZinderService) {
  }

  ngOnInit() {
  }

  clic() {
    const bodyCreateProfil = new BodyCreateProfilModel(this.nom, this.prenom, this.photoUrl, this.getInterets());
    this.service.createProfil(bodyCreateProfil).subscribe();
  }

  private getInterets(): string[] {
    const interets = [];
    interets.push(this.interet1);
    interets.push(this.interet2);
    interets.push(this.interet3);
    return interets;
  }

  change() {
    if (this.nom !== '' && this.prenom !== '') {
      this.disabled = '';
    }
  }
}
