import {Component, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinder.service';
import {MatchProfilModel} from '../../model/matchProfil.model';

@Component({
  selector: 'app-stats-liste',
  templateUrl: './stats-liste.component.html',
  styleUrls: ['./stats-liste.component.css']
})
export class StatsListeComponent implements OnInit {

  matchList: MatchProfilModel [] = [];

  constructor(private service: ZinderService) {
  }

  ngOnInit() {
    this.service.getMatch().subscribe(matchAPI => this.matchList = matchAPI);
  }

  delete(id: string) {
    this.service.deleteMatch(id).subscribe(() => {
      this.service.getMatch().subscribe(reponse => {
          this.matchList = reponse;
        }
      );
    });
  }
}
