import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsListeComponent } from './stats-liste.component';

describe('StatsListeComponent', () => {
  let component: StatsListeComponent;
  let fixture: ComponentFixture<StatsListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
