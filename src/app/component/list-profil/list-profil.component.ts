import {Component, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinder.service';
import {ProfilModel} from '../../model/profil.model';
import {InteretsModel} from '../../model/interets.model';

@Component({
  selector: 'app-list-profil',
  templateUrl: './list-profil.component.html',
  styleUrls: ['./list-profil.component.css']
})
export class ListProfilComponent implements OnInit {
  profils: ProfilModel [] = [];
  interets: InteretsModel [] = [];

  constructor(private service: ZinderService) {
  }

  ngOnInit() {
    this.service.getProfils().subscribe(profil => {
      this.profils = profil;
    });
    this.service.getInterets().subscribe(interets => this.interets = interets);
  }
}
