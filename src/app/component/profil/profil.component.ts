import {Component, Input, OnInit} from '@angular/core';
import {ProfilModel} from '../../model/profil.model';
import {InteretsModel} from '../../model/interets.model';
import {ZinderService} from '../../service/zinder.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  @ Input() profil: ProfilModel;
  @ Input() nom: string;
  @ Input() prenom: string;
  @ Input() photoUrl: string;
  @ Input() idInterets: string [];
  @ Input() listInterets: InteretsModel [] = [];

  constructor(private service: ZinderService) {
  }

  ngOnInit() {
    this.service.getInterets().subscribe();
    }
}
