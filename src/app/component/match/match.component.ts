import {Component, Input, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinder.service';
import {BodyMatchModel} from '../../model/bodyMatch.model';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  match: boolean;
  @Input() matchId: string;
  disabled = '';

  constructor(private service: ZinderService) {
  }

  ngOnInit() {
  }

  matching() {
    const bodyCreateMatch = new BodyMatchModel(true);
    this.service.createProfilMatch(this.matchId, bodyCreateMatch).subscribe();
    this.disabled = 'disabled';
  }

  dismatch() {
    const bodyCreateMatch = new BodyMatchModel(false);
    this.service.createProfilMatch(this.matchId, bodyCreateMatch).subscribe();
    this.disabled = 'disabled';
  }
}

