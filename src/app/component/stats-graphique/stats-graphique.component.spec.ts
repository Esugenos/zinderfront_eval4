import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsGraphiqueComponent } from './stats-graphique.component';

describe('StatsGraphiqueComponent', () => {
  let component: StatsGraphiqueComponent;
  let fixture: ComponentFixture<StatsGraphiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsGraphiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsGraphiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
