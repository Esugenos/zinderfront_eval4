import {Component, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinder.service';

@Component({
  selector: 'app-stats-graphique',
  templateUrl: './stats-graphique.component.html',
  styleUrls: ['./stats-graphique.component.css']
})
export class StatsGraphiqueComponent implements OnInit {

  constructor(private service: ZinderService) {
  }

  ngOnInit() {
    this.service.getMatch().subscribe();
  }

}
