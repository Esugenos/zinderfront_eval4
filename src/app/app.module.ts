import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ProfilComponent} from './component/profil/profil.component';
import {MatchComponent} from './component/match/match.component';
import {HomepageComponent} from './component/homepage/homepage.component';
import {ZinderService} from './service/zinder.service';
import {HttpClientModule} from '@angular/common/http';
import {ListProfilComponent} from './component/list-profil/list-profil.component';
import {StatsComponent} from './component/stats/stats.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import { StatsGraphiqueComponent } from './component/stats-graphique/stats-graphique.component';
import { StatsListeComponent } from './component/stats-liste/stats-liste.component';
import { AdminComponent } from './component/admin/admin.component';

const appRoutes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'stats', component: StatsComponent},
  {path: 'admin', component: AdminComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    ProfilComponent,
    MatchComponent,
    HomepageComponent,
    ListProfilComponent,
    StatsComponent,
    StatsGraphiqueComponent,
    StatsListeComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes),
    FormsModule
  ],
  providers: [ZinderService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
