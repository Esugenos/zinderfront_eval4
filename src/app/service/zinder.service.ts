import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProfilModel} from '../model/profil.model';
import {MatchProfilModel} from '../model/matchProfil.model';
import {InteretsModel} from '../model/interets.model';
import {BodyMatchModel} from '../model/bodyMatch.model';
import {BodyCreateProfilModel} from '../model/bodyCreateProfil.model';
import {map} from 'rxjs/operators';
import {ListProfilModel} from '../model/listProfil.model';

@Injectable()
export class ZinderService {

  constructor(private http: HttpClient) {
  }

  getProfils(): Observable<ProfilModel[]> {
    return this.http.get<ListProfilModel>('http://localhost:8088/zinder/profils').pipe(
      map(listProfils => listProfils.profils));
  }

  getMatch(): Observable<MatchProfilModel[]> {
    return this.http.get<MatchProfilModel[]>('http://localhost:8088/zinder/matchs');
  }

  getInterets(): Observable<InteretsModel[]> {
    return this.http.get<InteretsModel[]>('http://localhost:8088/zinder/interets');
  }

  deleteMatch(id: string): Observable<any> {
    return this.http.delete(`http://localhost:8088/zinder/matchs/${id}`);
  }

  createProfilMatch(id: string, body: BodyMatchModel): Observable<BodyMatchModel> {
    return this.http.post<MatchProfilModel>(`http://localhost:8088/zinder/profils/${id}/match`, body);
  }

  createProfil(body: BodyCreateProfilModel): Observable<ProfilModel> {
    return this.http.post<ProfilModel>('http://localhost:8088/zinder/profils', body);
  }
}

